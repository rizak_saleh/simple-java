package com.nostratech;
import java.util.*;
public class LiskedListExample {

	public static void main(String[] args) {
		List<String> list=new LinkedList<>();  
	    list.add("Rizak");  
	    list.add("Bayu");  
	    list.add("Abim");  
	    System.out.println("After adding: "+list);  
	    list.remove("Bayu");  
	    System.out.println("After removing: "+list);  
	    list.set(1,"Dian");  
	    System.out.println("After changing: "+list); 
	}

}
