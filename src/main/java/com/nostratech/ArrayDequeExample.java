package com.nostratech;
import java.util.*;
public class ArrayDequeExample {

	public static void main(String[] args) {
		//Creating Deque and adding elements  
		   Deque<String> deque = new ArrayDeque<String>();  
		   deque.add("Rizak");    
		   deque.add("Agung");     
		   deque.add("Dian");    
		   //Traversing elements  
		   for (String str : deque) {  
		   System.out.println(str);  
		   }  
	}

}
