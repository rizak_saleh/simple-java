package com.nostratech;

public interface NostraStack {

	public void push(int data);
	
	public int pop();
	
	public int max();
	
	public int size();
	
	public String print();
}


