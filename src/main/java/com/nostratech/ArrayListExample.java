package com.nostratech;
import java.util.*;
public class ArrayListExample {

	public static void main(String[] args) {
		//Creating a List  
		 List<String> list=new ArrayList<String>();  
		 //Adding elements in the List  
		 list.add("Lele");  
		 list.add("Paus");  
		 list.add("Hiu");  
		 list.add("Mas Koki");  
		 //Iterating the List element using for-each loop  
		 for(String animal:list)  
		  System.out.println(animal);  
	}
}
