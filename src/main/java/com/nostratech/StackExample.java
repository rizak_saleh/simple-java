package com.nostratech;
import java.util.Stack;
public class StackExample {

	public static void main(String[] args) {
		//creating an instance of Stack class  
		Stack<Integer> stk= new Stack<>();  
		// checking stack is empty or not  
		boolean result = stk.empty();  
		System.out.println("Is the stack empty? " + result);  
		// pushing elements into stack  
		stk.push(33);  
		stk.push(110);  
		stk.push(10);  
		stk.push(180);  
		//prints elements of the stack  
		System.out.println("Elements in Stack: " + stk);  
		result = stk.empty();  
		System.out.println("Is the stack empty? " + result);
	}

}
