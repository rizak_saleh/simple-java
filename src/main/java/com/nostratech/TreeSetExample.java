package com.nostratech;
import java.util.*;
class TreeSetExample {

	public static void main(String[] args) {
		//Creating and adding elements  
		  TreeSet<String> al=new TreeSet<String>();  
		  al.add("Andre");  
		  al.add("WIja");  
		  al.add("Maulana");  
		  al.add("Ajat");  
		  //Traversing elements  
		  Iterator<String> itr=al.iterator();  
		  while(itr.hasNext()){  
		   System.out.println(itr.next());  
		  }  
	}

}
