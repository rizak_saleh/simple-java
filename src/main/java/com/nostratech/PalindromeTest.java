package com.nostratech;

import java.util.*;
public class PalindromeTest {

	public static void main(String[] args) {
		String original, back = "";
		Scanner in = new Scanner (System.in);
		original = in.nextLine();
		int length = original.length();
		//Reverse from back to start char
		for (int i = length-1; i>=0; i--)
			back = back + original.charAt(i);
		if (original.equals(back))
			System.out.println("is Palindrome");
		else
			System.out.println("not Palindrome");

	}

}
