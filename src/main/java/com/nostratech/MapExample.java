package com.nostratech;
import java.util.*;
public class MapExample {

	public static void main(String[] args) {
		Map<Integer,String> map=new HashMap<Integer,String>();  
		  map.put(100,"Bayu");  
		  map.put(101,"Rizak");  
		  map.put(102,"Huda");  
		  //Elements can traverse in any order  
		  for(Map.Entry m:map.entrySet()){  
		   System.out.println(m.getKey()+" "+m.getValue());  
		  }  	

	}

}
