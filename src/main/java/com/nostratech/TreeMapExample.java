package com.nostratech;
import java.util.*;
public class TreeMapExample {

	public static void main(String[] args) {
		TreeMap<Integer,String> map=new TreeMap<Integer,String>();
		map.put(100,"Dian");
		map.put(102,"Rafi");
		map.put(101,"Wija");
		map.put(103,"Ayu");    
	        
		for(Map.Entry m:map.entrySet()){    
			System.out.println(m.getKey()+" "+m.getValue());    
	      }    

	}

}
