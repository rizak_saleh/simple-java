package com.nostratech2;

import java.util.Arrays;
public class SimpleStack implements NostraStack{

	private int[] dataArray;
	int top = -1;
	
	
	public SimpleStack() {
		dataArray = new int[50];
		
	}
	boolean isEmpty() {
		if (top == -1) {
			return true;
		}
		return false;
	}
	boolean isFull() {
		if(top == dataArray.length) {
			return true;
		}
		return false;
	}
	
	@Override
	public void push(int data) {
		if(!isFull()) {
			top ++; //top naik
			dataArray[top] = data;
			System.out.println("Push =" + data);
		}else {
			System.out.println("Stack is Full");
		}
	}

	@Override
	public int pop() {
		if(isEmpty()){
            int indexPop = top;
            top--; // TOP DI TURUNKAN
            System.out.println("Stack index " + indexPop  + " poped");
        }else{
            System.out.println("Empty stack");
        }
		return 0;
	}

	@Override
	public int max() {
		if(!isEmpty()) {
			int maxVal = dataArray[0];
			for (int i=1 ; i < dataArray.length; i++) {
				if(dataArray[i] > maxVal) {
					maxVal = dataArray[i];
				}
			}
			System.out.println("Max= " + maxVal);
			return maxVal;
		}
		return 0;
	}

	@Override
	public int size() {
		if(!isEmpty()) {
			System.out.print("Stack's size = ");
			for (int i=0 ; i <= top; i++) {
				System.out.println(dataArray[i] + " ");
			}
			System.out.println("");
		}else {
			System.out.println("Stack is Empty");
		}
		return 0;
	}

	@Override
	public String print() {
		
		return null;
	}

}
